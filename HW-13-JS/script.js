/*Реалізувати перемикання вкладок (таби) на чистому Javascript.*/

// const tabsTitle = document.querySelectorAll(".tabs-title");
// const tabsContent = document.querySelectorAll(".tab-content");

// for (let i = 0; i < tabsTitle.length; i++) {
//   tabsTitle[i].addEventListener("click", (event) => {
//     let currentTab = event.target.parentElement.children;
//     for (let j = 0; j < currentTab.length; j++) {
//       currentTab[j].classList.remove("active");
//     }
//     event.target.classList.add("active");
//     let currentContent = event.target.parentElement.nextElementSibling.children;
//     for (let a = 0; a < currentContent.length; a++) {
//       currentContent[a].classList.remove("active");
//       event.target.classList.add("active");
//     }
//     tabsContent[i].classList.add("active");
//   });
// }

//2 способ

const tabs = document.querySelector(".tabs");
const tabsContent = document.querySelectorAll(".tab-content");

tabs.addEventListener("click", (event) => {
  if (event.target.classList.contains("tabs-title")) {
    document.querySelectorAll(".tabs-title").forEach((tab) => {
      tab.classList.remove("active");
    });
    event.target.classList.add("active");
  }
  tabsContent.forEach((content) => {
    content.classList.remove("active");
    if (content.classList.contains(event.target.dataset.tab)) {
      content.classList.add("active");
    }
  });
});
