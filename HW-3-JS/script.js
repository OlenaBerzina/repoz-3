/*1. Попросіть користувача ввести свій вік за допомогою prompt. 
 Якщо вік менше 12 років, виведіть у вікні alert повідомлення про те, 
 що він є дитиною, якщо вік менше 18 років, виведіть повідомлення про те, 
 що він є підлітком, інакше виведіть повідомлення про те, що він є дорослим.
 */

let userAge = prompt("Введіть свій вік");

if (userAge === "" || userAge === null) {
  alert("Введіть значення");
} else {
  userAge = +userAge;
  if (isNaN(userAge)) {
    alert("Ввод не вірний!");
  } else if (userAge < 12) {
    alert("Нажаль, ви ще дитина");
  } else if (userAge < 18) {
    alert("Нажаль, ви є підлітком");
  } else if (userAge >= 18) {
    alert("Ласкаво просимо! Ви вже дорослий!");
  }

  // Варіант 2

  let userAge2 = prompt("Введіть свій вік");

  if (userAge2 === null || userAge2 === "" || isNaN(userAge2)) {
    alert("Ввод невірний");
  } else {
    switch (true) {
      case userAge2 < 12:
        alert("Нажаль, ви ще дитина");
        break;
      case userAge2 < 18:
        alert("Нажаль, ви є підлітком");
        break;
      case userAge2 >= 18:
        alert("Ласкаво просимо! Ви вже дорослий!");
        break;
      default:
        alert("Ви ввели не вірне значення");
    }

    /* Завдання з підвищенною складністю.
 Перевірте чи ввів користувач саме число в поле prompt. Якщо ні, то виведіть повідомлення, що введено не число.
 */

    let userInput = parseInt(prompt("Type your number"));
    if (isNaN(userInput)) {
      alert("Write a number!");
    } else {
      alert("It is ok!");
    }

    //  2. Напишіть програму, яка запитує у користувача місяць року (українською мовою маленкими літерами) та виводить повідомлення,
    //  скільки днів у цьому місяці. Результат виводиться в консоль.
    //  Скільки днів в кожному місяці можна знайти тут в розділі Юліанський і Григоріанський календарі - https://uk.wikipedia.org/wiki/%D0%9C%D1%96%D1%81%D1%8F%D1%86%D1%8C_(%D1%87%D0%B0%D1%81)
    //  (Використайте switch case)

    let monthOfYear = prompt("Введіть місяць поточного року").toLowerCase();

    switch (monthOfYear) {
      case "січень": {
        console.log("В цьому місяці 31 день");
        break;
      }
      case "лютий": {
        console.log("В цьому місяці 28 днів");
        break;
      }
      case "березень": {
        console.log("В цьому місяці 31 день");
        break;
      }
      case "квітень": {
        console.log("В цьому місяці 30 днів");
        break;
      }
      case "травень": {
        console.log("В цьому місяці 31 день");
        break;
      }
      case "червень": {
        console.log("В цьому місяці 30 днів");
        break;
      }
      case "липень": {
        console.log("В цьому місяці 31 день");
        break;
      }
      case "серпень": {
        console.log("В цьому місяці 31 день");
        break;
      }
      case "вересень": {
        console.log("В цьому місяці 30 днів");
        break;
      }
      case "жовтень": {
        console.log("В цьому місяці 30 днів");
        break;
      }
      case "листопад": {
        console.log("В цьому місяці 30 днів");
        break;
      }
      case "грудень": {
        console.log("В цьому місяці 31 день");
        break;
      }
      default:
        console.log("Ви ввели некоректне значення");
    }
  }
}
