/*
1. Запитайте у користувача два числа. Перевірте, чи є кожне з введених значень числом. 
Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом. 
Виведіть на екран всі числа від меншого до більшого за допомогою циклу for.
*/

let firstNumber;
let secondNumber;
while (true) {
  firstNumber = prompt("Введіть перше число");
  if (isNaN(firstNumber) || firstNumber === null || firstNumber.trim() === "") {
    alert("Ви не ввели число ! Введіть число !");
  } else {
    firstNumber = parseInt(firstNumber);
    alert("Ви ввели число:)");
    break;
  }
}
while (true) {
  secondNumber = prompt("Введіть друге число");
  if (
    isNaN(secondNumber) ||
    secondNumber === null ||
    secondNumber.trim() === ""
  ) {
    alert("Ви не ввели число ! Введіть число !");
  } else {
    secondNumber = parseInt(secondNumber);
    alert("Ви ввели число:)");
    break;
  }
}

let min = Math.min(firstNumber, secondNumber);
let max = Math.max(firstNumber, secondNumber);

for (let i = min; i <= max; i++) {
  console.log(i);
}

/*
2. Напишіть програму, яка запитує в користувача число та перевіряє, чи воно є парним числом. 
Якщо введене значення не є парним числом, то запитуйте число доки користувач не введе правильне значення.
*/

// 1 спосіб

while (true) {
  let evenNumber = prompt("Введіть парне число");
  if (evenNumber % 2 !== 0 || evenNumber === null || evenNumber === "") {
    alert("Ви ввели не парне число, або скасували ввод");
  } else {
    alert("Вірно!");
    break;
  }
}

// 2 спосіб

while (true) {
  let evenNumber = prompt("Введіть парне число");
  if (evenNumber % 2 === 0 && evenNumber !== null && evenNumber !== "") {
    alert("Супер!");
    break;
  } else {
    alert("Ви ввели не парне число");
  }
}
