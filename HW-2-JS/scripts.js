/* 
1. Створіть змінну "username" і присвойте їй ваше ім'я. Створіть змінну "password" і присвойте їй пароль (наприклад, "secret"). 
Далі ми імітуємо введеня паролю користувачем. Отримайте від користувача значення його паролю і перевірте, чи співпадає воно зі 
значенням в змінній "password". Виведіть результат порівнння в консоль.
*/

let userName = "Olena";
let password = "secret";
let userPassword = prompt(`Введите Ваш пароль`);

console.log(password === userPassword);
console.log(userPassword === null);
console.log(userPassword === "");
console.log(userPassword.length > 0);

/*
2. Створіть змінну x та присвойте їй значення 5. Створіть ще одну змінну y та запишіть присвойте їй значення 3.
Виведіть результати додавання, віднімання, множення та ділення
змінних x та y у вікні alert.
*/

let x = 5;
let y = 3;

alert(x + y);
alert(x - y);
alert(x * y);
alert(x / y);

//або

alert((x += y));
alert((x -= y));
alert((x /= y));
alert((x *= y));
