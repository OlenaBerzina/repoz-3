/*Технічні вимоги:*/

/*
Створити просту HTML-сторінку з кнопкою Знайти по IP.
Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
Усі запити на сервер необхідно виконати за допомогою async await.*/

const btn = document.getElementById("btn");

async function getIP() {
  try {
    const response = await fetch("https://api.ipify.org/?format=json");
    const data = await response.json();
    console.log(data);
    const ip = data.ip;

    const location = await fetch(`http://ip-api.com/json/${ip}`);
    const locationData = await location.json();

    const renderData = `
          <p><b>IP-адрес:</b> ${ip}</p>
          <p><b>Країна:</b> ${locationData.country}</p>
          <p><b>Регіон:</b> ${locationData.regionName}</p>
          <p><b>Місто:</b> ${locationData.city}</p>
          <p><b>Широта:</b> ${locationData.lat}</p>
          <p><b>Довгота:</b> ${locationData.lon}</p>
        `;
    document.getElementById("root").innerHTML = renderData;
  } catch (error) {
    console.error(error);
  }
}

btn.addEventListener("click", getIP);
