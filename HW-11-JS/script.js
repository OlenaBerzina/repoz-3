/*1. Додати новий абзац по кліку на кнопку:

По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" 
і додайте його до розділу <section id="content">*/

const btn = document.getElementById("btn-click");
const section = document.getElementById("content");
btn.style.border = "2px solid red";
btn.style.padding = "10px";
btn.style.backgroundColor = "yellow";
btn.style.display = "block";
btn.style.margin = "0 auto";
btn.style.marginBottom = "15px";

btn.addEventListener("click", () => {
  const paragraf = document.createElement("p");
  paragraf.textContent = "New Paragraph";
  console.log(paragraf);

  section.appendChild(paragraf);
});

/*2. Додати новий елемент форми із атрибутами:

Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.

По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути,
 наприклад, type, placeholder, і name. та додайте його під кнопкою.*/

const button = document.createElement("button");
button.id = "btn-input-create";
button.textContent = "Input";
button.style.cssText =
  "border: 2px solid red; padding: 10px; background-color: yellow; display: block; margin: 0 auto; margin-bottom: 15px";
const sectionMain = document.getElementById("main");
sectionMain.append(button);

button.addEventListener("click", () => {
  const newInput = document.createElement("input");
  newInput.type = "text";
  newInput.placeholder = "Enter your name";
  newInput.name = "userName";
  sectionMain.append(newInput);
});
