import React, {useState, useEffect} from 'react'
import Button from './Components/Button'
import styled from 'styled-components'
import GlobalStyle  from './Components/GlobalStyle';
import Modal from './Components/Modal';
import ModalHeader from './Components/ModalHeader';
import ModalBody from './Components/ModalBody';
import ModalFooter from './Components/ModalFooter';
import { sendRequest } from './helpers/sendRequest';
import ProductList from './Components/ProductList'
import Container from './Components/Container';
import { func } from 'prop-types';




const ButtonStyle = styled.button`
 background: transparent;
 border: none;
 margin-right: 12px;
 color: white;
 padding: 16px 20px 16px 20px;
 border-radius: 8px;
 font-size: 0;
`;

const DivStyle = styled.div`
background: grey;
padding: 15px 10px;
display: flex;
justify-content: flex-end;
`;

const modalWrap = styled.div`
width: 560px;
height: 558px;
background: white;
border-radius: 3px;
position: absolute;
top: 0;
left: 0;
display: flex;
justify-content: center;
align-items: center;

`

function App() {
  const [openModal, setOpenModal] = useState(false);
  const [openSecondModal, setOpenSecondModal] = useState(false);
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState(() => {
    return JSON.parse(localStorage.getItem("cart")) || [];
  });

  const [cartCount, setCartCount] = useState(cart.length);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [favorite, setFavorite] = useState([]);
  const [countFavorite, setCountFavorite] = useState(
    JSON.parse(localStorage.getItem("countFavorite")) || 0
  );
//Функции для счетчика избранное

  function addCounter() {
    setCountFavorite(countFavorite + 1)
  };

  function removeCounter() {
    if(countFavorite > 0) {
          setCountFavorite(countFavorite - 1)
    }
  };

  // Открываем модальное окно при клике на "Добавить в корзину"


const handleAddToCardClick = (product) => {
  const isInCart = cart.some(item => item.art === product.art);

  if (isInCart) {
      // Если товар уже в корзине, удаляем его сразу
      const updatedCart = cart.filter(item => item.art !== product.art);
      setCart(updatedCart);
      localStorage.setItem("cart", JSON.stringify(updatedCart));
  } else {
      // Если товара нет в корзине, открываем модалку
      setSelectedProduct(product);
      setOpenModal(true);
  }
};
// Обработчик для кнопки "YES" в модалке
const confirmAddToCart = () => {
  if (selectedProduct) {
    let updatedCart = [...cart];
    const isInCart = updatedCart.some(item => item.art === selectedProduct.art);

    if (isInCart) {
       // Удаляем товар из корзины
       updatedCart = updatedCart.filter(item => item.art !== selectedProduct.art);
    } else {
           // Добавляем товар в корзину
      updatedCart.push(selectedProduct);
    }

    setCart(updatedCart);
    setCartCount(updatedCart.length);
    localStorage.setItem('cart', JSON.stringify(updatedCart));
    setOpenModal(false);
  }
};



  //Обновляем localStorage при изменении избранного

  useEffect(() => {
    const savedFavorites = JSON.parse(localStorage.getItem("favorite")) || [];
    setFavorite(savedFavorites);

  }, []);

  useEffect(() => {
    localStorage.setItem('countFavorite', JSON.stringify(countFavorite));
  }, [countFavorite]);


  useEffect(() => {
    const savedCard = JSON.parse(localStorage.getItem('cart')) || [];
    setCart(savedCard);
  }, []);

  //Обновляем localStorage при изменении корзины

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);

  

  useEffect(() => {
    sendRequest('/goods.json').then((data) => setProducts(data));
  }, []);

  return (
    <>
  <DivStyle className='app'> 
  <GlobalStyle />
  {/* <ButtonStyle type="button" className="btn" onClick={() => setOpenModal(true)}> Open first modal</ButtonStyle>
  <ButtonStyle type='button' className="btn" onClick={() => setOpenSecondModal(true) }> Open second modal</ButtonStyle> */}
  <ButtonStyle type="button" className="btn"> 
    <img className ="cart-icon" src="/public/shopping-cart.png"></img>
    <span className='cart-content'>{cart.length}</span>
  </ButtonStyle>
  <ButtonStyle type='button' className="btn"> 
    <img className="like-icon" src="/public/like.png"></img>
    <span className='like-content'>{countFavorite}</span>
  </ButtonStyle>
  </DivStyle>


<Modal 

isOpen={openModal} onClose={() => setOpenModal(false)}>
<ModalHeader>
<button className="close-btn" >
<img src="Icon.svg" width={15} height={15}/>
</button>
  <div className='content'></div>
</ModalHeader>
<ModalBody>
<h2 className="title">Add Product "{selectedProduct?.name}"</h2> 
<p className="wrap-content">By clicking the “Yes", you will add this good to the card.</p>
</ModalBody>
<ModalFooter>

<div className="btn-wrapper">
 <button type='button' className="left-btn" onClick={confirmAddToCart}>YES, ADD</button>
 <button type='button' className="right-btn" onClick={() => setOpenModal(false)}>NO, CANCEL</button>
 </div>
</ModalFooter>
</Modal>


<Modal height="250px" isOpen={openSecondModal} 
onClose={() => setOpenSecondModal(false)}>

<ModalHeader>
<button className="close-btn" onClick={() => setOpenModal(false)}>
<img src="Icon.svg" width={15} height={15}/>
</button>
  
</ModalHeader>
<ModalBody>
<h2 className="title">Add Product “NAME”</h2> 
<p className="wrap-content">Description for you product.</p>
</ModalBody>
<ModalFooter>

<div className="btn-wrapper">
 <button type='button' className="left-btn" onClick={() => console.log('click')}>ADD TO FAVORITE</button>

 </div>
</ModalFooter>
</Modal>
<Container>

<div>
  <h1 className='title-phone'>Каталог товаров</h1>
  <ProductList products={products} handleAddToCardClick={handleAddToCardClick} confirmAddToCart={confirmAddToCart} addCounter={addCounter} removeCounter={removeCounter} cart={cart} />
</div>

</Container>
</>

  )
}

export default App



