import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';


function ProductCard ({product, handleAddToCardClick, addCounter, removeCounter, confirmAddToCart, cart = []}) {

const {name, price, img, art, colour} = product;
const [isInCart, setIsInCart] = useState(false);
useEffect(() => {
    setIsInCart(cart.some(item => item.art === product.art));
}, [cart]);

//Функция для обработки клика
const handleClick = () => {
    handleAddToCardClick(product);

};

const [isFavorite, setIsFavorite] = useState(() => {
    const savedFavorites = JSON.parse(localStorage.getItem('favorites')) || [];
  
    return savedFavorites.includes(art);
     
});



  // Функция добавления/удаления из избранного
const toggleFavorite = () => {


    setIsFavorite(!isFavorite);
    let favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    if(!isFavorite) {
        favorites.push(art);
        addCounter();
    } else {

        favorites = favorites.filter((id) => id !== art);
        removeCounter();
        
    }
    localStorage.setItem('favorites', JSON.stringify(favorites));
    // localStorage.setItem('countFavorite', JSON.stringify(countFavorite + (isFavorite ? -1 : 1)));
    
};


return (
    <div className='product-card'>
        <button className='add-favorite' onClick={toggleFavorite}>
            {isFavorite ? "★" : "☆"} 
        </button>
        <img className="card-img" src={img} alt={name} />
        <h3 className='card-title'>{name}</h3>
        <p className='card-price'>Цена: {price} грн.</p>
        <p className='card-color'>Цвет: {colour}</p>
        <button className='add-card' onClick={handleClick}> 
            {isInCart ? "Удалить из корзины" : "Добавить в корзину"}
            </button>
    </div>
); 
};

ProductCard.PropTypes = {
    product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        img: PropTypes.string.isRequired,
        art: PropTypes.number.isRequired,
        colour: PropTypes.string.isRequired,
    }).isRequired,
};

export default ProductCard