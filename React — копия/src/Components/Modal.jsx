import React from "react";



function Modal ({isOpen, onClose, children, height = '350px'}) {
if (!isOpen) return null;

const onWrapperClick = (e) => {
    if (e.target === e.currentTarget) onClose();
};

return (
<div className="modal-wrapper" onClick={onWrapperClick}>
<div className="modal" 
onClick={(e) => e.stopPropagation() }
style={{height}}
>{children}
<button className="close-btn" onClick={onClose}>
<img src="Icon.svg" width={15} height={15}/>
</button>
</div>
</div>
)
}




export default Modal

