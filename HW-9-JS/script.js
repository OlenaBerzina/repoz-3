/*1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift" та обчисліть кількість рядків з довжиною більше за 3 символи. 
Вивести це число в консоль.*/

/*1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
Використайте 2 способи для пошуку елементів.*/

const feature = document.getElementsByClassName("feature");
// console.log(feature);
for (let i = 0; i < feature.length; i++) {
  feature[i].style.textAlign = "center";
}

const feature2 = document.querySelectorAll("feature");
console.log(feature);

//2. Змініть текст усіх елементів h2 на "Awesome feature".

//1 спосіб

const mainTitle = document.querySelectorAll("h2");
mainTitle.forEach((title) => (title.textContent = "Awesome feature"));

//2 спосіб

const mainTitle = document.getElementsByTagName("h2");
for (let i = 0; i < mainTitle.length; i++) {
  mainTitle[i].textContent = "Awesome feature";
}

//3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".*/

const exclamatory = document.querySelectorAll(".feature-title");
for (let i = 0; i < exclamatory.length; i++) {
  exclamatory[i].textContent += " !";
}
