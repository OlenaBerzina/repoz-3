/*Практичне завдання 1:

-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, 
щоб змінити текстовий вміст елемента div через затримку 3 секунди. 
Новий текст повинен вказувати, що операція виконана успішно.*/

const btn = document.createElement("button");
const divElement = document.createElement("div");
document.body.append(btn);
document.body.append(divElement);
divElement.style.cssText = "text-align: center";
btn.style.cssText =
  "padding: 20px; width: 100px; height: 20px; background-color: yellow; color: dark; border-radius: 10px; margin: 40px auto; cursor: pointer; display: block; line-height: 5px";
btn.textContent = "Click me";

btn.addEventListener("click", (event) => {
  setTimeout(() => {
    divElement.textContent = "Операція виконана успішно";
  }, 3000);
});

/*Практичне завдання 2:

Реалізуйте таймер зворотного відліку, використовуючи setInterval. 
При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. 
Після досягнення 1 змініть текст на "Зворотній відлік завершено".*/

const divText = document.createElement("div");
document.body.append(divText);
divText.textContent = "10";
divText.style.cssText =
  "padding: 20px; width: 200px; height: 45px; background-color: yellow; color: dark; border-radius: 10px; margin: 40px auto; border: 1px solid black; font-size: 28px; text-align: center; ";

const timeout = 1000;
let i = 10;
const intervalID = setInterval(() => {
  i--;
  divText.textContent = i;
  if (i === 1) {
    divText.textContent = "Зворотній відлік завершено";
    clearInterval(intervalID);
  }
}, timeout);
