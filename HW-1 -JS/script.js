//1. Оголосіть змінну і присвойте в неї число. Перевірте, чи ця змінна має тип "number" і виведіть результат в консоль.

const newNumber = 125;
console.log(typeof newNumber);
console.log(newNumber);

//  2. Оголосіть змінну name і присвойте їй ваше ім'я. Оголосіть змінну lastName і присвойте в неї Ваше прізвище.
//  Виведіть повідомлення у консоль у форматі `Мене звати {ім'я}, {прізвище}` використовуючи ці змінні.

let name = "Elena";
let lastName = "Berzina";
console.log(`Мене звати ${name} ${lastName}`);

//  3. Оголосіть змінну з числовим значенням і виведіть в консоль її значення всередині рядка.

const newTask = "100";
console.log(newTask);
