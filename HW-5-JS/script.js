// 1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.

function getNumber(a, b) {
  if (b === 0) {
    return "Ділення на 0 неможливе";
  }
  return a / b;
}
let result = getNumber(15, 5);
console.log(result);

/*
2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.

Технічні вимоги:
- Отримати за допомогою модального вікна браузера два числа. Провалідувати отримані значення(перевірити, що отримано числа). 
Якщо користувач ввів не число, запитувати до тих пір, поки не введе число

- Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /. 
Провалідувати отримане значення. Якщо користувач ввів не передбачене значення, вивести alert('Такої операції не існує').

- Створити функцію, в яку передати два значення та операцію.

- Вивести у консоль результат виконання функції.
*/

function add(numberOne, numberTwo) {
  return numberOne + numberTwo;
}
function subtraction(numberOne, numberTwo) {
  return numberOne - numberTwo;
}

function multiplication(numberOne, numberTwo) {
  return numberOne * numberTwo;
}
function division(numberOne, numberTwo) {
  return numberOne / numberTwo;
}
let userFirstInput, userSecondInput, userOperate;
while (true) {
  userFirstInput = prompt("Введіть перше число");
  if (userFirstInput === null) {
    alert("Ви скасували ввод");
    break;
  }
  if (userFirstInput.trim() === "") {
    alert("Ви нічого не ввели");
    continue;
  }

  userFirstInput = Number(userFirstInput);

  if (isNaN(userFirstInput)) {
    alert("Ви ввели не число. Введіть число !");
    continue;
  } else {
    alert("Вірно!");
    break;
  }
}
while (true) {
  userOperate = prompt("Введіть математичну операцію: * / + -");
  if (userOperate === null) {
    alert("Ви скасували ввод");
    break;
  }
  if (
    userOperate !== "*" &&
    userOperate !== "-" &&
    userOperate !== "/" &&
    userOperate !== "+"
  ) {
    alert("Такої операції не існує");
    continue;
  } else {
    alert("Вірно!");
    break;
  }
}
while (true) {
  userSecondInput = prompt("Введіть друге число");
  if (userSecondInput === null) {
    alert("Ви скасували ввод");
    break;
  }
  if (userSecondInput.trim() === "") {
    alert("Ви ввели порожній рядок");
    continue;
  }
  userSecondInput = Number(userSecondInput);

  if (isNaN(userSecondInput)) {
    alert("Ви ввели не число. Введіть число !");
    continue;
  } else {
    alert("Вірно!");
    break;
  }
}
function calculate(userFirstInput, userSecondInput, operate) {
  switch (operate) {
    case "+":
      return add(userFirstInput, userSecondInput);
    case "-":
      return subtraction(userFirstInput, userSecondInput);
    case "*":
      return multiplication(userFirstInput, userSecondInput);
    case "/":
      return division(userFirstInput, userSecondInput);

      break;
  }
}
alert(calculate(userFirstInput, userSecondInput, userOperate).toFixed(2));
console.log(calculate(userFirstInput, userSecondInput, userOperate));

/*3. Опціонально. Завдання:

Реалізувати функцію підрахунку факторіалу числа.

Технічні вимоги:

- Отримати за допомогою модального вікна браузера число, яке введе користувач.

- За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.

- Використовувати синтаксис ES6 для роботи зі змінними та функціями.*/

let userInput = prompt("Enter a number");

function factorial(userInput) {
  let result = 1;
  for (let i = 2; i <= userInput; i++) {
    result *= i;
  }
  return result;
}
alert(factorial(userInput));
