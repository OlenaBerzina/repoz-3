1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

Відповідь: для створення нового DOM-елементу використовують метод document.createElenent('name of element');
const newDiv = document.createElement('div');
newDiv.textContent = 'Це новий елемент';
document.body.appand(newDiv).

elementAppendChild - додає елемент в кінець дочірніх елементів батьківського елемента;
const newLi = document.createElement('li');
newLi.textContent = 'Це новий елемент ли';
const ul = document.querySelector('ul');
ul.appendChild(li);

element.append() - дозволяє додати декілька елементів або строк одночасно. Відрізняється від AppendChild тим, що може додавати не тільки вузли, а і текстові строки
const newSpan = document.createElement('span');
newSpan.textContent = 'Це новий спан';
document.body.append(span, 'some text')

element.prepend() - дозволяє додати новий елемент або текст перед усіма дочірніми елементами. Працює аналогічно append(), тільки додає нові елементи в начало, не в кінець
parent.prepend('li');

element.innerHTML - дозволяє додати HTML розмітку, як строку
const container = document.querySelector('.container');
container.innerHTML = '<p>Додаємо текст разом з розміткою</p>';

element.insertAdjacentHTML() - вставляє HTML код у вказане місце відносно конкретного елемента (до початку елемента - beforebegin, після початку - afterbegin, до кінця - beforeend або після кінця елементу - afterend)
const div = document.querySelector('.container');
div insertAdjasentHTML('beforeend', '<p>Новий параграф</p>');

element.insertBefore() - вставляє елемент в DOM перед вказаним дочірнім елементом
const newLi = document.createElement('li');
const ul = document.querySelector('ul');
const firstLi = ul.querySelector('li')// перший елемент списку
ul.insertBefore(newLi, firstLi); //вставляємо новий елемент перед першим елементом

2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

Відповідь: Створюємо змінну та находимо елемент з класом navigation на сторінці
const removeEl = document.querySelector('.navigation')
використовуємо метод remove для видалення елемента
removeEl.remove()

Також можна перевірити, чи дійсно цей елемент існує на сторінці, якщо так, то видалити його

const removeEl = document.querySelector('.navigation');
if(removeEl) {
removeEl.remove();
}

3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

Відповідь:
insertBefore() - дозволяє вставити елемент перед іншим вказаним елементом
parentElement.insertBefore(newElem, referenceElem);
parentElement - батьківський елемент, до якого буде доданий новий елемент
newElem - елемент, який треба додати
referenceElem - елемент, перед яким буде додано новий елемент

insertAdjacentElement() - дозволяє додати елемент до або після іншого елемента, має 4 мет позиции для вставки:
beforebegin - перед самим елементом
afterbegin - внутрі елемента, на початок
beforeend - внутрі елементу в кінець
afterend - після елементу

insertAdjacentHTML() - цей метод працює аналогічно insertAdjacentElement(), але вставляє не елемент, а HTML розмітку (строку);

insertAdjacentText() - цей метод вставляє тільки текст, не HTML код

after() i before() - дозволяє додати елементи прямо перед або після вибраного елементу. Відрізняється від метода insertBefore(), тим що працює з самим елементом, а не з його батьком.
