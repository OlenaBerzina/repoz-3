/*1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". 
Додайте цей елемент в footer після параграфу.*/

const newEl = document.createElement("a");
newEl.textContent = "Learn More";
newEl.href = "#";
const paragraf = document.querySelector(".feature-txt");
paragraf.append(newEl);

/*2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".

Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.

Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.

Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.

Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.*/

const newSelect = document.createElement("select");
newSelect.id = "rating";
const main = document.querySelector("main");
main.prepend(newSelect);

const optionData = [
  { value: 4, text: "4 Stars" },
  { value: 3, text: "3 Stars" },
  { value: 2, text: "2 Stars" },
  { value: 1, text: "1 Star" },
];

for (let i = 0; i < optionData.length; i++) {
  const newOption = document.createElement("option");
  newOption.value = optionData[i].value;
  newOption.textContent = optionData[i].text;
  newSelect.append(newOption);
}
