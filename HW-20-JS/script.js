/*Технічні вимоги:
Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни.
Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з 
властивості characters.
Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер 
епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.*/

const filmsUrl = "https://ajax.test-danit.com/api/swapi/films";

function fetchData(url) {
  return fetch(url).then((response) => {
    if (!response.ok) {
      throw new Error(`Error: ${response.status}`);
    }

    return response.json();
  });
}

function renderMovieAndChatacters() {
  fetchData(filmsUrl)
    .then((films) => {
      console.log(films);
      const container = document.getElementById("movies-container");
      films.forEach((film) => {
        const filmElement = document.createElement("div");
        filmElement.className = "film";
        filmElement.innerHTML = `
    <h2>Эпизод ${film.episodeId}: ${film.name}</h2>
    <p>${film.openingCrawl}</p>
    <ul class='characters-list'></ul>
    `;
        container.append(filmElement);

        const charactersList = filmElement.querySelector(".characters-list");
        const characterPromises = film.characters.map((characterUrl) =>
          fetchData(characterUrl)
        );
        Promise.all(characterPromises)
          .then((characters) => {
            characters.forEach((character) => {
              const characterElement = document.createElement("li");
              characterElement.textContent = character.name;
              charactersList.append(characterElement);
            });
          })
          .catch((error) => {
            charactersList.textContent = "Error";
            console.error(`Error ${film.name}:`, error);
          });
      });
    })
    .catch((error) => {
      console.log(`Mistake`, error);
    });
}
renderMovieAndChatacters();

//Вариант 2

// const root = document.querySelector("#root");
// const listOfFilms = document.createElement("ul");

// fetch("https://ajax.test-danit.com/api/swapi/films")
//   .then((response) => {
//     if (!response.ok) {
//       throw new Error(`Error happend here, ${response.status}`);
//     }
//     return response.json();
//   })
//   .then((data) => {
//     console.log(data);
//     data.forEach((el) => {
//       const li = document.createElement("li");
//       const liName = `Name: ${el.name}`;
//       const liEpisode = `Episode: ${el.episodeId}`;
//       const liDescr = `Description: ${el.openingCrawl}`;
//       li.textContent = `${liName}. ${liEpisode}. ${liDescr}`;
//       listOfFilms.append(li);

//       const paragraf = document.createElement("p");
//       paragraf.textContent = "Characters:";

//       el.characters.forEach((urlOfCharact) => {
//         fetch(urlOfCharact)
//           .then((response) => {
//             if (!response.ok) {
//               throw new Error("Error", response.status);
//             }
//             return response.json();
//           })
//           .then((character) => {
//             paragraf.textContent = paragraf.textContent + `${character.name}`;
//             li.append(paragraf);
//           });
//       });
//     });
//   });
// root.append(listOfFilms);

//Preloader
window.onload = function () {
  let preloader = document.getElementById("preloader");
  preloader.classList.add("hide-preloader");
  setTimeout(function () {
    preloader.classList.add("preloader-hidden");
  }, 5000);
};
