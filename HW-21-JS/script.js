/*Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.
Технічні вимоги:
При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. 
Для цього потрібно надіслати GET запит на наступні дві адреси:
https://ajax.test-danit.com/api/json/users
https://ajax.test-danit.com/api/json/posts
Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
Кожна публікація має бути відображена у вигляді картки (приклад в папці), та включати заголовок, текст, а також ім'я, 
прізвище та імейл користувача, який її розмістив.
На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї 
необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження 
із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. 
Це нормально, все так і має працювати.
Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності 
ви можете додавати також інші класи.
Необов'язкове завдання підвищеної складності
Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження. Анімацію можна використовувати 
будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно, в якому користувач зможе 
ввести заголовок та текст публікації. Після створення публікації дані про неї необхідно надіслати в POST запиті на адресу: 
https://ajax.test-danit.com/api/json/posts. Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку). 
Автором можна присвоїти публікації користувача з id: 1.
Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки для підтвердження змін необхідно надіслати PUT 
запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.
*/
// class Card {
//   constructor(users, posts) {
//     this.users = users;
//     this.posts = posts;
//   }
//   //метод для создания и отображения карточки
//   render() {
//     const cardContainer = document.createElement("div");
//     cardContainer.style.cssText =
//       "margin-bottom: 20px; border: 5px solid grey; box-shadow: 10px 5px 15px grey; width: 900px; padding: 30px; background-color: #E2E6E9";

//     cardContainer.innerHTML = `
//     <h2>${this.users.name} (${this.users.email})</h2>
//     `;
//     //создаем список постов для пользователя
//     const listOfPosts = document.createElement("ul");
//     this.posts.forEach((post) => {
//       const liPosts = document.createElement("li");
//       liPosts.style.marginTop = "15px";
//       liPosts.innerHTML = `<strong>${post.title}</strong><br> ${post.body}
//       `;
//       //Созааем кнопку удаления

//       const deleteBtn = document.createElement("button");

//       deleteBtn.textContent = "Delete post";
//       deleteBtn.style.cssText =
//         "cursor: pointer; display: flex; margin-top: 20px;";

//       deleteBtn.addEventListener("click", () => {
//         this.deletePost(post.id, liPosts);
//       });
//       liPosts.append(deleteBtn);

//       const edit = document.createElement("img");
//       edit.src = "./img/pencil.svg";
//       edit.style.cssText =
//         "width: 20px; height: 20px; margin-left: 110px; transform: translateY(-90%)";
//       listOfPosts.append(liPosts, edit);
//     });

//     cardContainer.append(listOfPosts);
//     return cardContainer;
//   }
//   //Метод для удаления поста

//   deletePost(postId, postElement) {
//     fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
//       method: "DELETE",
//     })
//       .then((response) => {
//         if (!response.ok) {
//           throw new Error(
//             `Mistake, you cannot delete this post: ${response.status}`
//           );
//         }
//         //удалаем єлемент поста
//         postElement.remove();
//       })
//       .catch((error) => {
//         console.error("Mistake", error.message);
//       });
//   }
// }

// //основной контейнер для вывода карточек
// const root = document.querySelector("#root");
// const userPostContainer = document.createElement("div");
// //функция для отображения всех карточек

// function renderUsersandPosts(users, posts) {
//   users.forEach((user) => {
//     const userPost = posts.filter((post) => post.userId === user.id);
//     //Создаем екземпляр карточки

//     const card = new Card(user, userPost);

//     //добавляем карточку в контейнер
//     userPostContainer.append(card.render());
//   });

//   //добавляем весь контейнер в корень
//   root.append(userPostContainer);
// }

// Promise.all([
//   fetch("https://ajax.test-danit.com/api/json/users").then((response) => {
//     if (!response.ok) {
//       throw new Error(`Error, ${response.status}`);
//     }
//     return response.json();
//   }),

//   fetch("https://ajax.test-danit.com/api/json/posts").then((response) => {
//     if (!response.ok) {
//       throw new Error(`Error, ${response.status}`);
//     }
//     console.log(response);
//     return response.json();
//   }),
// ])

//   .then(([users, posts]) => {
//     console.log(users);
//     console.log(posts);
//     renderUsersandPosts(users, posts);
//   })
//   .catch((error) => {
//     console.error(`Mistake:`, error.message);
//   });

/*-----*/

const usersUrl = "https://ajax.test-danit.com/api/json/users";
const postsUrl = "https://ajax.test-danit.com/api/json/posts";

const postContainer = document.getElementById("postContainer");
const modal = document.getElementById("modal");
const postForm = document.getElementById("postForm");
const modalTitle = document.querySelector(".modal-title");
const addPostBtn = document.getElementById("addPostBtn");
const closePostBtn = document.getElementById("closeModal");

let users = [];
let posts = [];
let editingPostId = null;

class Card {
  constructor(post, user) {
    this.post = post;
    this.user = user;
  }

  createCard() {
    const card = document.createElement("div");
    card.classList.add("card");
    card.innerHTML = `
    <h3>${this.post.title}</h3>
     <p>${this.post.body}</p>
     <p><strong>${this.user.name}</strong> (${this.user.email})</p>
     <button class="deleteBtn">Delete post</button>
     <button class="editBtn">Edit post</button>
      
    `;
    card.style.cssText =
      "margin-bottom: 20px; border: 5px solid grey; box-shadow: 10px 5px 15px grey; width: 900px; padding: 30px; background-color: #E2E6E9";
    const deleteBtn = card.querySelector(".deleteBtn");
    const editBtn = card.querySelector(".editBtn");
    if (deleteBtn) {
      deleteBtn.addEventListener("click", () => this.deletePost(card));
    } else {
      console.error("Not found");
    }
    if (editBtn) {
      editBtn.addEventListener("click", () => this.editCard());
    } else {
      console.error("Not found");
    }

    return card;
  }

  deletePost(card) {
    // console.log(`delete ${this.post.id}`);
    fetch(`${postsUrl}/${this.post.id}`, {
      method: "DELETE",
    })
      .then((res) => {
        if (res.ok) {
          console.log("success");
          card.remove();
        } else {
          console.error("Failed");
        }
      })
      .catch((err) => console.error("Mistake, you cannot delete this", err));
  }
  editCard() {
    // console.log(`edit ${this.post.id}`);
    modal.classList.remove("hidden");
    modalTitle.textContent = "Edit this post";
    document.getElementById("postTitle").value = this.post.title;
    document.getElementById("postBody").value = this.post.body;
    editingPostId = this.post.id;
  }
}
function renderPosts() {
  postContainer.innerHTML = "";
  posts.forEach((post) => {
    const user = users.find((u) => u.id === post.userId);
    if (user) {
      const card = new Card(post, user).createCard();
      postContainer.append(card);
    }
  });
}

function fetchData() {
  Promise.all([fetch(usersUrl), fetch(postsUrl)])
    .then(([usersResp, postsResp]) =>
      Promise.all([usersResp.json(), postsResp.json()])
    )
    .then(([usersData, postsData]) => {
      users = usersData;
      posts = postsData;

      renderPosts();
    })
    .catch((err) => console.error("Mistake of loading:", err));
}

function openModal() {
  modal.classList.remove("hidden");
  modalTitle.textContent = "Add a new post";
  postForm.reset();
  editingPostId = null;
}

function closeModal() {
  modal.classList.add("hidden");
}

postForm.addEventListener("submit", (e) => {
  e.preventDefault();
  const title = document.getElementById("postTitle").value;
  const body = document.getElementById("postBody").value;

  const method = editingPostId ? "PUT" : "POST";
  const url = editingPostId ? `${postsUrl}/${editingPostId}` : postsUrl;
  const newPost = {
    title,
    body,
    userId: 1,
  };
  fetch(url, {
    method,
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(newPost),
  })
    .then((res) => res.json())
    .then((data) => {
      if (editingPostId) {
        posts = posts.map((post) => (post.id === editingPostId ? data : post));
      } else {
        posts.unshift(data);
      }
      renderPosts();
      closeModal();
    })
    .catch((err) => console.error("Mistake of saving", err));
});

addPostBtn.addEventListener("click", openModal);
closePostBtn.addEventListener("click", closeModal);

fetchData();

//3

// const usersUrl = "https://ajax.test-danit.com/api/json/users";
// const postsUrl = "https://ajax.test-danit.com/api/json/posts";

// const postContainer = document.getElementById("postContainer");
// const modal = document.getElementById("modal");
// const postForm = document.getElementById("postForm");
// const modalTitle = document.querySelector(".modal-title");
// const addPostBtn = document.getElementById("addPostBtn");
// const closePostBtn = document.getElementById("closeModal");

// let users = [];
// let posts = [];
// let editingPostId = null;

// class Card {
//   constructor(post, user) {
//     this.post = post;
//     this.user = user;
//   }

//   createCard() {
//     const card = document.createElement("div");
//     card.classList.add("card");
//     card.dataset.id = this.post.id;
//     card.innerHTML = `
//     <h3>${this.post.title}</h3>
//      <p>${this.post.body}</p>
//      <p><strong>${this.user.name}</strong> (${this.user.email})</p>
//      <button class="deleteBtn">Delete post</button>
//      <button class="editBtn">Edit post</button>
//     `;

//     const deleteBtn = card.querySelector(".deleteBtn");
//     const editBtn = card.querySelector(".editBtn");

//     return card;
//   }

//   deletePost(card) {
//     console.log(`delete ${this.post.id}`);
//     fetch(`${postsUrl}/${this.post.id}`, {
//       method: "DELETE",
//     })
//       .then((res) => {
//         if (res.ok) {
//           console.log("success");
//           card.remove();
//         } else {
//           console.error("Failed");
//         }
//       })
//       .catch((err) => console.error("Mistake, you cannot delete this", err));
//   }
//   editCard() {
//     console.log(`edit ${this.post.id}`);
//     modal.classList.remove("hidden");
//     modalTitle.textContent = "Edit this post";
//     document.getElementById("postTitle").value = this.post.title;
//     document.getElementById("postBody").value = this.post.body;
//     editingPostId = this.post.id;
//   }
// }
// function renderPosts() {
//   postContainer.innerHTML = "";
//   posts.forEach((post) => {
//     const user = users.find((u) => u.id === post.userId);
//     if (user) {
//       const card = new Card(post, user).createCard();
//       postContainer.append(card);
//     }
//   });
// }

// function fetchData() {
//   Promise.all([fetch(usersUrl), fetch(postsUrl)])
//     .then(([usersResp, postsResp]) =>
//       Promise.all([usersResp.json(), postsResp.json()])
//     )
//     .then(([usersData, postsData]) => {
//       users = usersData;
//       posts = postsData;

//       renderPosts();
//     })
//     .catch((err) => console.error("Mistake of loading:", err));
// }

// function openModal() {
//   modal.classList.remove("hidden");
//   modalTitle.textContent = "Add a new post";
//   postForm.reset();
//   editingPostId = null;
// }

// function closeModal() {
//   modal.classList.add("hidden");
// }

// postForm.addEventListener("submit", (e) => {
//   e.preventDefault();
//   const title = document.getElementById("postTitle").value;
//   const body = document.getElementById("postBody").value;

//   const method = editingPostId ? "PUT" : "POST";
//   const url = editingPostId ? `${postsUrl}/${editingPostId}` : postsUrl;
//   const newPost = {
//     title,
//     body,
//     userId: 1,
//   };
//   fetch(url, {
//     method,
//     headers: { "Content-Type": "application/json" },
//     body: JSON.stringify(newPost),
//   })
//     .then((res) => res.json())
//     .then((data) => {
//       if (editingPostId) {
//         posts = posts.map((post) => (post.id === editingPostId ? data : post));
//       } else {
//         posts.unshift(data);
//       }
//       renderPosts();
//       closeModal();
//     })
//     .catch((err) => console.error("Mistake of saving", err));
// });

// addPostBtn.addEventListener("click", openModal);
// closePostBtn.addEventListener("click", closeModal);

// fetchData();
// postContainer.addEventListener("click", (e) => {
//   if (e.target.classList.contains("deleteBtn")) {
//     const card = e.target.closest(".card");
//     const postId = card.dataset.id;
//     console.log(`Delete post with ID: ${postId}`);
//     fetch(`${postsUrl}/${postId}`, { method: "DELETE" })
//       .then((res) => {
//         if (res.ok) {
//           card.remove();
//         }
//       })
//       .catch((err) => console.error("Error deleting post:", err));
//   }

//   if (e.target.classList.contains("editBtn")) {
//     const card = e.target.closest(".card");
//     const postId = card.dataset.id;
//     console.log(`Edit post with ID: ${postId}`);
//     const post = posts.find((p) => p.id === Number(postId));
//     if (post) {
//       modal.classList.remove("hidden");
//       modalTitle.textContent = "Edit this post";
//       document.getElementById("postTitle").value = post.title;
//       document.getElementById("postBody").value = post.body;
//       editingPostId = postId;
//     }
//   }
// });
