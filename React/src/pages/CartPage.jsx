import React, {useEffect, useState} from 'react';
import ProductCard from '../Components/ProductCard';

function CartPage() {
    const [cart, setCart] 
    
    = useState(() => {
        return JSON.parse(localStorage.getItem('cart')) || [];
    });

    const [quantities, setQuantities] = useState({});

    // Обновляем количество при изменении корзины

    useEffect(() => {
        if (!Array.isArray(cart)) return;

        const initialQuantities = {};
        cart.forEach((item) => {
            initialQuantities[item.art] = item.quantity || 1;
        });
        setQuantities(initialQuantities);
    }, [cart]);

     // Функция увеличения количества товара

     const increaseQuantity = (art) => {
        setQuantities((prev) => ({...prev, [art]: prev[art] + 1}));
            
        };
          // Функция уменьшения количества товара
        const decreaseQuantity = (art) => {
            setQuantities((prev) => {
                const updatedQuantity = prev[art] > 1 ? prev[art] - 1 : 1;
                return {...prev, [art]: updatedQuantity};
            });
        };
          // Функция удаления товара
 
   const removeFromCart = (art) => {
       const updatedCart = cart.filter((item) => item.art !== art);
       setCart(updatedCart);
       localStorage.setItem('cart', JSON.stringify(updatedCart));
   };

     // Подсчет общей суммы

     const totalPrice = Array.isArray(cart) ? cart.reduce((acc, item) => acc + item.price * (quantities[item.art] || 1), 0) : 0;

   

return (
        <div>
            <h1 className="cart-title" >Корзина</h1>
            <div className="cart-list">

                {cart.length > 0 ? (
                    cart.map((product) => (
               
               
                        <div className="cart-wrapper">
                             <div key={product.art} className='cart-item'>
                            <img src={product.img} alt={product.name} className='product-image' />
                            <div className='product-info'>
                                <h3 className='product-name'>{product.name}</h3>
                                <p>Цена: {product.price} грн</p>
                                <p>Цвет: {product.colour}</p>
                                <div className='btn-box'>
                                    <button className="btn-cart" onClick={() => increaseQuantity(product.art)}>+</button>
                                    <span>{quantities[product.art]}</span>
                                    <button className="btn-cart" onClick={() => decreaseQuantity(product.art)}>-</button>
                                </div>
                                
                            </div>
                            <p className='product-sum'><strong>Сумма: </strong>{product.price * (quantities[product.art] || 1)} грн</p>
                            <hr className='product-line'/>
                        </div>
                         <button className='remove-btn'>
                                <img src="../../public/img/free-icon-delete.png" className='remove-img' onClick={() => removeFromCart(product.art)} />
                            </button>
                        </div>
                    ))
                ) : (
                    <p>Корзина пуста</p>
                )}
       
            </div>
            <div className='cart-summary'>
                <h2>Общая сумма: {totalPrice} грн</h2>
                <button className='cart-order' onClick={() => console.log('тут будет форма заказа')}>Оформить заказ</button>
            </div>
        </div>
    )

    }
    

    




export default CartPage