import React from "react";
import ProductCard from '../Components/ProductCard';
import PropTypes from 'prop-types';


//Этот компонент будет рендерить список товаров

function ProductList ({products, handleAddToCardClick, addCounter, removeCounter, confirmAddToCart, cart}) {

    return (

            <div className="product-list">
            {Array.isArray(products) && products.length > 0 ? (
                products.map((product) => (
                    <ProductCard 
                    key={product.art} 
                    product={product} 
                    handleAddToCardClick={handleAddToCardClick}
                    addCounter={addCounter}
                    removeCounter={removeCounter}
                    confirmAddToCart={confirmAddToCart}
                    cart={cart}
                    
                  
                     />
                ))
            ) : (
                <p>Товары загружаются...</p>
            )}
        </div>
        
    );
};

ProductList.PropTypes = {
    products: PropTypes.array.isRequired,
    addToCart: PropTypes.func.isRequired,
    favorite: PropTypes.array.isRequired,
};

export default ProductList;