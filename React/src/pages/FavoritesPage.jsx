import React, {useState, useEffect} from "react";
import ProductCard from "../Components/ProductCard";

function FavoritesPage() {
    const[favorites, setFavorites] = useState(() => {
        return JSON.parse(localStorage.getItem('favorites')) || [];
    });

    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites));
    }, [favorites]);

    const removeFromFavorites = (art) => {
        const updatedFavorites = favorites.filter((item) => item.art !== art);
        setFavorites(updatedFavorites);
        localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
    };

    return (
<div>
    <h1 className="favorite-title">Избранное</h1>
    <div className="favorites-list">
        {favorites.length > 0 ? (
            favorites.map((product) => (
              <div key={product.art} className="favorite-item">
                <p className="favorites-name"><strong>{product.name}</strong></p>
                <img src={product.img} alt={product.name} className="favorite-img" />
                
              </div>
            ))
        ) : (
            <p>Список избранного пуст</p>
        )}
    </div>
</div>
    )
}

export default FavoritesPage;
