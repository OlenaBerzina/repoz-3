import React from "react";

function ModalHeader({ children, onClose }) {
  
    return (
      <div className="modal-header">
         <button className="close-btn" onClick={onClose}></button>
        {children}
      </div>
    );
  }
  
  export default ModalHeader;


