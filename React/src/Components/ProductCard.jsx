import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';


function ProductCard ({product, handleAddToCardClick, addCounter, removeCounter, confirmAddToCart, onRemove,
    cart = []}) {

        const { name, price, img, art, colour } = product;
        const [isInCart, setIsInCart] = useState(false);
        
        
        useEffect(() => {
            try {
                const cart = JSON.parse(localStorage.getItem('cart')) || [];
                const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
        
                setIsInCart(Array.isArray(cart) && cart.some(item => item.art === product.art));
                setIsFavorite(Array.isArray(favorites) && favorites.some(item => item.art === product.art));
            } catch (error) {
                console.error("Ошибка при чтении данных из localStorage:", error);
                setIsInCart(false);
                setIsFavorite(false);
            }
        },  [art]); 



//Функция для обработки клика
const handleClick = () => {
    handleAddToCardClick(product);

};

const [isFavorite, setIsFavorite] = useState(() => {
    try {
        const storedFavorites = localStorage.getItem('favorites');
        const savedFavorites = storedFavorites ? JSON.parse(storedFavorites) : [];
        
        return Array.isArray(savedFavorites) && savedFavorites.some(item => item.art === art);
    } catch (e) {
        console.error("Ошибка при чтении избранного из localStorage:", error);
        return false; // Если произошла ошибка, избранное считается неактивным
    }
});

  // Функция добавления/удаления из избранного
const toggleFavorite = () => {


    setIsFavorite(!isFavorite);
    let favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    if(!isFavorite) {
        favorites.push(art);
        addCounter();
    } else {

        favorites = favorites.filter((id) => id !== art);
        removeCounter();
        
    }
    localStorage.setItem('favorites', JSON.stringify(favorites));
    
    
let updatedFavorites = JSON.parse(localStorage.getItem('favorites')) || [];
if (isFavorite) {
    updatedFavorites = updatedFavorites.filter((item) => item.art !== art);
} else {
    updatedFavorites.push(product);
}
localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
setIsFavorite(!isFavorite);

};

const handleRemove = () => {
    if (onRemove) {
        onRemove(art);
    }
};


return (
    <div className='product-card'>
        <button className='add-favorite' onClick={toggleFavorite}>
            {isFavorite ? "★" : "☆"} 
        </button>
        <img className="card-img" src={img} alt={name} />
        <h3 className='card-title'>{name}</h3>
        <p className='card-price'>Цена: {price} грн.</p>
        <p className='card-color'>Цвет: {colour}</p>
        {onRemove && (
            <button className='remove-btn' onClick={handleRemove}>
                <img src='../../public/shopping-cart.png'></img>
            </button>
        )}
        <button className='add-card' onClick={handleClick}> 
            {isInCart ? "Удалить из корзины" : "Добавить в корзину"}
            </button>
    </div>
); 
};

ProductCard.PropTypes = {
    product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        img: PropTypes.string.isRequired,
        art: PropTypes.number.isRequired,
        colour: PropTypes.string.isRequired,
    }).isRequired,
};

export default ProductCard;