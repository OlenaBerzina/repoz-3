import React from "react";

function ModalFooter({ children }) {
    return (
      <div className="modal-body">
          {children}
      </div>
      
      
    );
  }
  
  export default ModalFooter;