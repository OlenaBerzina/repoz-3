/*Технічні вимоги:*/

/*Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.

Технічні вимоги:

При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. 
Для цього потрібно надіслати GET запит на наступні дві адреси:

https://ajax.test-danit.com/api/json/users
https://ajax.test-danit.com/api/json/posts

Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
Кожна публікація має бути відображена у вигляді картки (приклад в папці), та включати заголовок, текст, а також ім'я, 
прізвище та імейл користувача, який її розмістив.
На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї 
необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження 
із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. 
Це нормально, все так і має працювати.
Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності 
ви можете додавати також інші класи.


Необов'язкове завдання підвищеної складності

Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження. Анімацію можна 
використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно, в якому користувач 
зможе ввести заголовок та текст публікації. Після створення публікації дані про неї необхідно надіслати в POST запиті на адресу:  
https://ajax.test-danit.com/api/json/posts. Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному 
порядку). Автором можна присвоїти публікації користувача з id: 1.
Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки для підтвердження змін необхідно надіслати 
PUT запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.*/

class Card {
  constructor(post, user) {
    this.post = post;
    this.user = user;
  }

  createCardElement() {
    const card = document.createElement("div");
    card.classList.add("card");
    card.dataset.postId = this.post.id;
    card.style.cssText = `border: 10px solid grey; box-shadow: 10px 5px 15px grey; padding: 30px"`;
    card.innerHTML = `
    <h2>${this.post.title}</h2>
    <p>${this.post.body}</p>
    <p>${this.user.name}. (${this.user.email})</p>
    <button class="deleteBtn">Delete post</button>
    <button class="editBtn">Edit post</button>

    `;
    return card;
  }
}
const postContainer = document.getElementById("post-container");

function fetchAndDisplayPosts() {
  Promise.all([
    fetch("https://ajax.test-danit.com/api/json/posts").then((response) =>
      response.json()
    ),
    fetch("https://ajax.test-danit.com/api/json/users").then((response) =>
      response.json()
    ),
  ])
    .then(([posts, users]) => {
      console.log("Users:", users);
      console.log("Posts:", posts);
      postContainer.innerHTML = "";
      posts.forEach((post) => {
        const user = users.find((user) => user.id === post.userId);
        if (!user) {
          console.error(`User with ID ${post.userID} not found`);
          return;
        }
        const card = new Card(post, user);
        postContainer.append(card.createCardElement());
      });
    })
    .catch((error) => console.error("Mistake of loading", error));
}

//добавление поста

postForm.addEventListener("submit", (e) => {
  e.preventDefault();
  const title = document.getElementById("postTitle").value;
  const body = document.getElementById("postBody").value;

  const newPost = {
    title,
    body,
    userId: 1,
  };

  fetch("https://ajax.test-danit.com/api/json/posts", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(newPost),
  })
    .then((res) => {
      if (!res.ok) {
        throw new Error(`Failed: ${res.status}`);
      }
      return res.json();
    })

    .then((data) => {
      console.log("Новый пост успешно добавлен:", data);
      const user = { id: 1, name: "Author Name", email: "author@example.com" };
      const card = new Card(data, user);
      postContainer.prepend(card.createCardElement());
      postForm.reset();
      closeModal();
    })
    .catch((error) => console.error("Mistake of saving", error));
});

// удаление поста

function deletePost(postId, cardElement) {
  fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
    method: "DELETE",
  })
    .then((response) => {
      if (response.ok) {
        cardElement.remove();
      } else {
        console.error(`Failed to delete ${postId}: ${response.status}`);
      }
    })
    .catch((error) => console.error("Error", error));
}
//функция для редактирования поста

function editPost(postId, updatedData, cardElement) {
  fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
    method: "PUT",
    headers: { "Content-type": "application/json" },
    body: JSON.stringify(updatedData),
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error(`Failed to update: ${response.status}`);
      }
    })
    .then((updatedPost) => {
      console.log("Post updated:", updatedPost);

      //обновляем содержимое карточки на странице
      cardElement.querySelector(".card-title").textContent = updatedPost.title;
      cardElement.querySelector(".card-body").textContent = updatedPost.body;

      closeModal();
    })
    .catch((error) => console.error("Error during edit", error));
}

//форма редактирования поста

const editForm = document.getElementById("editForm");
let currentEditCard = null;

postContainer.addEventListener("click", (event) => {
  if (event.target.classList.contains("deleteBtn")) {
    const card = event.target.closest(".card");
    const postId = card.dataset.postId;
    if (!postId) {
      console.error("Not found");
      return;
    }
    deletePost(postId, card);
  }
  if (event.target.classList.contains("editBtn")) {
    const card = event.target.closest(".card");
    const postId = card.dataset.postId;
    if (!postId) {
      console.error("Error");
      return;
    }
    const modal = document.getElementById("modal");
    modal.style.display = "block";

    document.getElementById("editTitle").value =
      card.querySelector(".card-title").textContent;
    document.getElementById("editBody").value =
      card.querySelector(".card-body").textContent;
    currentEditCard = { cardElement: card, postId };
  }
});
editForm.addEventListener("submit", (event) => {
  event.preventDefault();
  if (!currentEditCard) {
    console.error("Not selected card");
    return;
  }
  const updateTitle = document.getElementById("editTitle").value;
  const updateBody = document.getElementById("editBody").value;

  const updateData = {
    title: updateTitle,
    body: updateBody,
    userId: 1,
  };
  editPost(currentEditCard.postId, updateData, currentEditCard.cardElement);
});

function openModal() {
  const modal = document.getElementById("modal");
  modal.style.display = "block";
}

function closeModal() {
  const modal = document.getElementById("modal");
  modal.style.display = "none";
}
const btnAddPost = document.getElementById("addPostBtn");
const btnCloseModal = document.getElementById("closeModal");

btnAddPost.addEventListener("click", openModal);
btnCloseModal.addEventListener("click", closeModal);

fetchAndDisplayPosts();

window.onload = function () {
  let preloader = document.getElementById("preloader");
  preloader.classList.add("hide-preloader");
  setTimeout(function () {
    preloader.classList.add("preloader-hidden");
  });
};
