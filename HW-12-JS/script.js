/*Реалізувати функцію підсвічування клавіш.


Технічні вимоги:
- У файлі index.html лежить розмітка для кнопок.
- Кожна кнопка містить назву клавіші на клавіатурі
- Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. 
При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням 
Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а 
кнопка Enter знову стає чорною.
Але якщо при натискані на кнопку її не існує в розмітці, то попередня активна кнопка повина стати неактивною.*/

const btns = document.querySelectorAll(".btn");
document.addEventListener("keydown", (event) => {
  btns.forEach((btn) => {
    btn.style.backgroundColor = "black";
  });
  btns.forEach((btn) => {
    const btnText = btn.textContent.toLowerCase();
    const keyPressed = event.key.toLowerCase();
    if (btnText === keyPressed) {
      event.preventDefault();
      btn.style.backgroundColor = "blue";
    }
    if (btnText !== keyPressed) {
      btn.style.backgroundColor = "black";
    }
  });
});
