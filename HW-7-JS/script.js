/*1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, 
якщо рядок є паліндромом
(читається однаково зліва направо і справа наліво), або false в іншому випадку.
*/

function isPalindrome(str) {
  str = str.toLowerCase().trim();
  return str === str.split("").reverse().join("");
}
result = isPalindrome("Анна");
console.log(result);
result1 = isPalindrome("крокодил");
console.log(result1);

/*
2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити, максимальну довжину 
і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. Ця функція стане в нагоді для 
валідації форми. Приклади використання функції:
// Рядок коротше 20 символів
funcName('checked string', 20); // true
// Довжина рядка дорівнює 18 символів
funcName('checked string', 10); // false
*/

function funcName(str, maxLength) {
  if (str.length <= maxLength) {
    return true;
  } else {
    return false;
  }
}

result = funcName("checked string", 20);
console.log(result);
result2 = funcName("checked string", 10);
console.log(result2);
/*
3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt. 
Функція повина повертати значення повних років на дату виклику функцію.*/

//Рішення, якщо користувач введе дату народження з використанням коми між значеннями

function getAge() {
  let now = new Date();
  let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
  let userBrInput = prompt(
    "Введіть свою дату народження у форматі: рік, місяць, день"
  );
  let userBirthday = userBrInput.split(",").map(Number);
  if (
    userBirthday.length !== 3 ||
    isNaN(userBirthday[0]) ||
    isNaN(userBirthday[1]) ||
    isNaN(userBirthday[2])
  ) {
    alert("Ви помилились(: Введіть дату в форматі: рік, місяць, число");
    return;
  }
  let userBirthDate = new Date(
    userBirthday[0],
    userBirthday[1] - 1,
    userBirthday[2]
  );

  let age = today.getFullYear() - userBirthDate.getFullYear();
  if (
    today.getMonth() < userBirthDate.getMonth() ||
    (today.getMonth() === userBirthDate.getMonth() &&
      today.getDate() < userBirthDate.getDate())
  ) {
    age = age - 1;
  }
  return age;
}

let age = getAge();
alert(`Ваш вік ${age} років`);

//Рішення № 2, якщо користувач введе свою дату народження в форматі РРРРММДД без коми між значеннями

function getFullAge() {
  let now = new Date();
  let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
  let userBrInput = prompt(
    "Введіть свою дату народження в форматі: РРРРММДД. Наприклад 19901110"
  );

  if (userBrInput.length !== 8 || isNaN(userBrInput)) {
    alert("Ви помилилися! ВВедіть дату в форматі: РРРРММДД");
    return;
  }
  let year = parseInt(userBrInput.slice(0, 4), 10);
  let month = parseInt(userBrInput.slice(4, 6), 10);
  let data = parseInt(userBrInput.slice(6, 8), 10);
  if (month > 12 || data > 31) {
    alert("Місяць повинен бути менше 13, а дата повинна бути менше 32");
    return;
  }
  month = month - 1;
  let userBirthdayDay = new Date(year, month, data);
  let age = today.getFullYear() - userBirthdayDay.getFullYear();
  if (
    today.getMonth() < userBirthdayDay.getMonth() ||
    (today.getMonth() === userBirthdayDay.getMonth() &&
      today.getDate() < userBirthdayDay.getDate())
  ) {
    age--;
  }
  return age;
}
let age = getFullAge();
alert(`Ваш повний вік ${age} років`);
