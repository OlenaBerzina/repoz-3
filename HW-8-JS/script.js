/*1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift" та обчисліть кількість рядків з довжиною більше за 3 символи. 
Вивести це число в консоль.*/

const words = ["travel", "hello", "eat", "ski", "lift"];
words.forEach(function (item) {
  if (item.length > 3) {
    console.log(item);
  }
});

/*2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}. 
Наповніть різними даними. Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча".
Відфільтрований масив виведіть в консоль.*/

const people = [
  { name: "Ivan", age: 25, sex: "man" },
  { name: "Olga", age: 27, sex: "woman" },
  { name: "Victor", age: 38, sex: "man" },
  { name: "Sergey", age: 32, sex: "man" },
];

const filteredPeople = people.filter((person) => person.sex === "man");
console.log(filteredPeople);

/*3. Реалізувати функцію фільтру масиву за вказаним типом даних. (Опціональне завдання)

Технічні вимоги:

- Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме 
будь-які дані, другий аргумент - тип даних.

- Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком 
тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], 
і другим аргументом передати 'string', то функція поверне масив [23, null].*/

//метод 1

function filterBy(array, type) {
  return array.filter((item) => typeof item !== type);
}
const data = ["cat", 45, null, 107, true, "horse"];
const result = filterBy(data, "number");
console.log(result);

//метод 2

function filterBy(array, type) {
  const result = [];
  for (let i = 0; i < array.length; i++) {
    if (typeof array[i] !== type) {
      result.push(array[i]);
    }
  }
  return result;
}
const array = ["cat", 45, null, 107, true, "horse"];
const result = filterBy(array, "number");
console.log(result);
