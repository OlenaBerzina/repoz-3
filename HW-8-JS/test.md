1. Опишіть своїми словами як працює метод forEach.

Відповідь: forEach - це метод масива, який запускає цикл. forEach перебирає масив та виконує функцію, яка в свою чергу виконує вказані дії з кожним елементом масива. Тобто, коли викликається метод forEach, він починає послідовно перебирати кожен елемент масива. Далі для кожно елемента масива виконується Call-back функція, в яку передають 3 аргументи: елемент, індекс і сам масив. В середині Call-back функції можна виконувати вказані операції з елементами (змінити значення, вивести в консоль і т.д.).

array.forEach(function(item, index, arr){
some code
})
console.log(item);

2. Які методи для роботи з масивом мутують існуючий масив, а які повертають новий масив? Наведіть приклади.

Відповідь:
array.concat() - поєднує один або кілька масивів в новий масив. Не змінює даний масив, а замість цього повертає поверхневу копію ісходного масива;
array.entries() - повертає новий об'єкт ітератор, що дозволяє перебирати масив по парах [index, value];
array.filter() - створює новий масив, що містить усі елементи вихідного масиву, які задовільняють умові в Call-back функції та повертає новий масив;
array.forEach() - не мутує вихідний масив, але якщо напряму змінити елементи внутрі функції обратного виклику, масив може змінитися;
array.from() - створює новий масив з масивоподібного або ітеруємого об'єкту;
array.join(separator) - не мутує вихідний масив. Він створює нову строку об'єднуючи всі елементи масива через вказаний роздільник;
array.map(callback) -створює новий масив, застосовуючи вказану функцію к кожному елементу ісходного масива;
array.of() - створює новий масив з вказаними елементами;
array.pop() - видалає останній елемент масива та повертає його;
array.reduse() - не мутує вихідний масив, використовується для того, щоб привести масив к єдиному значенню;
array.reverse() - мутує вихідний масив. Перевертає порядок елементів в масиві, та повертає той же змінений масив;
array.shift() - мутує вихідний масив, видаляючи його перший елемент;
array.slice() - не мутує вихідний масив, повертає новий масив, який містить копію частини вихідного масиву;
array.sort() - мутує вихідний масив, сортуючи його на місті;
array.splice() - мутує вихідний масив, так як дозволяє додавати, видаляти або замінювати елементи. Він змінює сам масив та повертає масив видалених елементів;
array.unshift() - мутує вихідний масив, додаючи один або декілька елементів в початок масиву.

3. Як можна перевірити, що та чи інша змінна є масивом?

Відповідь: використати метод Array.isArray()
const arr = [1, 5, 7]
const arr = Array.isArray(arr); //true
const notArr = ['not array'];
const notArr = Array.isArray(notArr); //false

4. В яких випадках краще використовувати метод map(), а в яких forEach()?

Відповідь: forEach та цикл .map - це два методи, які дозволяють ітерувати (перебирати) елементи масиву. forEach використовують коли потрібно виконати якусь дію з кожним елементом масиву але не потрібно створювати новий масив.

const numbers = [4, 5, 8, 95];
numbers.forEach(number => {
number \* 2;
})
console.log(number);

.map() - створює новий масив, застосовуючи задану функцію до кожного елементу вихідного масиву. Використовують коли потрібно створити новий масив на основі існуючого, змінюючи значення кожного елемента.

const numbers = [4, 5, 8, 95];
const doubleNumbers = numbers.map(number => number \* 3)
console.log(doubleNumbers);
