/*Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, 
щоб ці характеристики заповнювалися під час створення об'єкта.
Створіть гетери та сеттери для цих властивостей.
Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.*/

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(value) {
    if (value.length > 0) {
      this._name = value;
    } else {
      concole.error("The name cannot be en empty");
    }
  }
  get age() {
    return this._age;
  }
  set age(value) {
    if (value > 0) {
      this._age = value;
    } else {
      console.error("The age cannot be less than 0");
    }
  }
  get salary() {
    return this._salary;
  }
  set salary(value) {
    if (value > 0) {
      this._salary = value;
    } else {
      console.error("The salary cannot be less than 0");
    }
  }
}
class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }
  get lang() {
    return this._lang;
  }
  set lang(value) {
    if (Array.isArray(value)) {
      this._lang = value;
    } else {
      console.error("Error");
    }
  }
  get salary() {
    return super.salary * 3;
  }
}
const employee = new Employee("Ivan", 25, 25000);
const programmer = new Programmer("Vlad", 24, 28000, ["JS", "Pyton"]);
console.log(programmer);
const programmer1 = new Programmer("Igor", 28, 29000, ["C++", "Pyton"]);
console.log(programmer1);
const programmer2 = new Programmer("Olga", 31, 35000, ["JS", "Pyton", "C++"]);
console.log(programmer2);
console.log(programmer.salary);
console.log(programmer1.salary);
console.log(programmer2.salary);
