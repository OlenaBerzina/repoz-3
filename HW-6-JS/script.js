/*1. Створіть об'єкт product з властивостями name, price та discount. 
Додайте метод для виведення повної ціни товару з урахуванням знижки. 
Викличте цей метод та результат виведіть в консоль.*/

let product = {
  name: "Lenovo",
  price: 22000,
  discount: 10, //в процентах

  getPrice: function () {
    let finalPrice = 0;
    finalPrice = this.price - (this.price * this.discount) / 100;
    return finalPrice;
  },
};

console.log(`Ціна товару з урахуванням знижки:`, product.getPrice());
/*
2. Напишіть функцію greeting, яка приймає об'єкт з властивостями name та age, 
і повертає рядок з привітанням і віком, наприклад "Привіт, мені 30 років".
Попросіть користувача ввести своє ім'я та вік за допомогою prompt, і викличте 
функцію gteeting з введеними даними(передавши їх як аргументи). Результат виклику 
функції виведіть з допомогою alert.
*/

function greeting(name, age) {
  return `Привіт, ${name} тобі ${age} років !`;
}
let userName;
let userAge;

while (true) {
  userName = prompt(`Введіть своє ім'я`);
  if (
    userName.trim() === "" ||
    userName === null ||
    !/^[a-zA-Zа-яА-ЯїЇєЄіІґҐ']+$/.test(userName)
  ) {
    alert("Не Вірно! Введіть ім'я");
    continue;
  }
  while (true) {
    userAge = prompt("Введіть свій вік");
    if (userAge === null || userAge.trim() === "" || isNaN(userAge)) {
      alert("Помилка, введіть ваш вік");
      continue;
    }
    userAge = Number(userAge);
    break;
  }
  break;
}

let result = greeting(userName, userAge);
alert(result);

/*
3.Опціональне. Завдання:

Реалізувати повне клонування об'єкта.

Технічні вимоги:
- Написати функцію для рекурсивного повного клонування об'єкта (без єдиної передачі за посиланням, внутрішня 
вкладеність властивостей об'єкта може бути досить великою).

- Функція має успішно копіювати властивості у вигляді об'єктів та масивів на будь-якому рівні вкладеності.

- У коді не можна використовувати вбудовані механізми клонування, такі як функція Object.assign() або spread.*/

let car = {
  brand: "Toyota",
  model: "Camry",
  year: 2023,
  color: "silver",
  price: 25000,
  isElectric: false,

  engine: {
    type: "бензиновый",
    volume: 2.5,
    power: 200,
  },
  wheels: {
    size: 17,
    material: "алюминий",
  },
  options: ["кондиционер", "круиз-контроль", "панорамная крыша"],
};

function deepClone(obj) {
  let cloned = {};
  for (i in obj)
    cloned[i] = typeof obj[i] == "object" ? deepClone(obj[i]) : obj[i];
  return cloned;
}

let car2 = deepClone(car);
car2.engine.volume = 3;
console.log(car);
console.log(car2);
car2.year = 2024;
console.log(car);
console.log(car2);
car2.options[2] = "без панорамной крыши";
console.log(car);
console.log(car2);
